

<?php
            // Validation on server side to make sure form is commpleted
            // var messages for checking validation
                $error = ""; $successMessage = "";
             // process validcation if POST is selected
                if($_POST){
                    // check weather the email section is filled in.
                    if (!$_POST["email"]) {

                        $error .= "An email address is required.<br>";

                    }
                    // check weather the content section is filled in.
                    if (!$_POST["content"]) {

                        $error .= "The content field is required.<br>";

                    }
                    // check weather the subject section is filled in.
                    if (!$_POST["subject"]) {

                        $error .= "The subject is required.<br>";

                    }
                    // check weather a file has been uploaded.
                    if (!$_POST["songToUpload"]) {

                        $error .= "File Upload is required required.<br>";

                    }
                    if ($error != "") {

                        $error = '<div class="alert alert-danger" role="alert"><p>There were error(s) in your form:</p>' . $error . '</div>';

                    }

                    header("localhost". $_SERVER['http://localhost:8888/real-php-projects/php-project-bootstrap/php-music-contact-form/']);
                    exit();
                }


            // Only process the form if $_POST isn't empty
            if( ! empty($_POST) ) {

                // var for connecting to database
                $link = mysqli_connect('localhost', 'Sam', 'password', 'songs_and_names');

                // error check to make sure connection is valid
                if (mysqli_connect_error()) {
                    die ('There was an error connecting to the database'.$link->mysqli_connect_errno. ':' .$link->connect_error);
                }

                // sql command to call the database and table to insert
                $sql = "INSERT INTO People (Email, Subject, Message) VALUES ('{$link->real_escape_string($_POST['email'])}','{$link->real_escape_string($_POST['subject'])}',
    '{$link->real_escape_string($_POST['message'])}')";
                $sql2 = "INSERT INTO Songs(Track) VALUES ('{$link->real_escape_string($_POST['songToUpload'])}')";

                // insert data inside the database
                $insert = $link->query($sql);
                $insert2 = $link->query($sql2);

                // Check and see if the data has been inserted
                if($insert && $insert2 ){
                    echo "Success! Row ID:{$link->insert_id}";
                }else{
                    die("Error: {$link->errno} : {$link->error}");
                }

                // close your connection
                $link->close();

            }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Latest version of Internet Explorer   -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">

    <title>Music is the spice of life</title>
</head>

<body>
<div class="container">
    <h1>Get In Touch With Sam About Music Products</h1>
    <!-- where the javascript error statement appears-->
    <div id="error"><? echo $error.$successMessage; ?></div>





<!-- user infromation form -->
    <form method="post">
        <feildset class="form-group">
            <label> Email Address</label>
            <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email">
            <small class="text-muted">Promise....We will never share your email</small>
        </feildset>
        <br/>
        <feildset class="form-group">
            <label> Subject</label>
            <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Email">
        </feildset>
        <feildset class="form-group">
            <label> Message</label>
            <textarea type="text" class="form-control" id="message" name="message" rows="3"></textarea>
        </feildset>
        <feildset class="form-group">
            <label for="exampleFormControlFile1">Upload those sweet tracks here!!!</label>
            <input type="file"  name="songToUpload" id="fileToUpload">
        </feildset>
        <br />
        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
    </form>



<!--  User music file upload   -->





<!-- Optional JavaScript -->

<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<!-- Ajax outside scripts incoporated from source-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>

<!--javascript version of checking form-->
    <script type="text/javascript">

        $("form").submit(function(e) {

            var error = "";
            // check and prompt if user filled in email correctly
            if ($("#email").val() == "") {

                error += "The email field is required.<br>"

            }
            // check and prompt if user filled in subject correctly
            if ($("#subject").val() == "") {

                error += "The subject field is required.<br>"

            }
            // check and prompt if user filled in message correctly
            if ($("#message").val() == "") {

                error += "The content field is required.<br>"

            }

            if($("#fileToUpload").val() ==""){
                error += "<p>You need to upload a file before submitting.</p>";
            }
            // check and prompt if user filled in email correctly
            if (error != "") {

                $("#error").html('<div class="alert alert-danger" role="alert"><p><strong>There were error(s) in your form:</strong></p>' + error + '</div>');

                return false;

            } else {

                return true;

            }
        })

    </script>

</body>
</html>
